provider "aws" {
    region  = "eu-west-3"
    access_key = 
    secret_key =
}

variable "login_DockerR" {
  type = string
}

variable "md_DockerR" {
  type = string
}
resource "aws_key_pair" "my_ec2" {
    key_name   = "terraform-key"
    public_key = file("./terraform.pub")
}

resource "aws_security_group" "instance_sg1" {
  name = "terraform-test-sg1"
  
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 5000
        to_port = 5000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_instance" "my_ec21" {
    key_name      = aws_key_pair.my_ec2.key_name
    ami           = "ami-0d6aecf0f0425f42a"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instance_sg1.id]
    connection {
        type        = "ssh"
        user        = "ubuntu"
        private_key = file("./terraform")
        host        = self.public_ip
    }

       provisioner "file" {
        source      = "registry.sh"
        destination = "registry.sh"
    }
    
    provisioner "local-exec" {
    command = "echo ${aws_instance.my_ec21.public_ip}:5000> ip_address.txt"
}

    provisioner "local-exec" {
    command = "echo ${var.login_DockerR}>>ip_address.txt"
}

    provisioner "local-exec" {
    command = "echo ${var.md_DockerR}>>ip_address.txt"
    
}





       provisioner "file" {
        source      = "ip_address.txt"
        destination = "ip_address.txt"
    }
 
    provisioner "remote-exec" {
     
          inline = [
          " sudo sh registry.sh $(awk 'NR==2' ip_address.txt) $(awk 'NR==3' ip_address.txt)",
          ]
    } 

tags = {
        Name = "RegestryM"
    }

}

output "Registry_ip" {
    value = aws_instance.my_ec21.public_ip
}



resource "aws_security_group" "instance_sg_stages" {
  name = "terraform-Stages-sg"
  
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }



    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        }
        
          ingress {
        from_port = 4243
        to_port = 4243
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

          ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "Stages" {
    key_name      = aws_key_pair.my_ec2.key_name
    ami           = "ami-0d6aecf0f0425f42a"
    instance_type = "t2.small"
    vpc_security_group_ids = [aws_security_group.instance_sg_stages.id]
    connection {
        type        = "ssh"
        user        = "ubuntu"
        private_key = file("./terraform")
        host        = self.public_ip
    }
    
            provisioner "local-exec" {
    command = "echo ${aws_instance.Stages.public_ip}:4243> ip_address1.txt"
}
    
       provisioner "file" {
        source      = "Stages.sh"
        destination = "Stages.sh"
    }
    
    
     provisioner "file" {
        source      = "ip_address.txt"
        destination = "ip_address.txt"
    }
    

    
       provisioner "remote-exec" {
     
          inline = [
          "sudo sh Stages.sh $(awk 'NR==1' ip_address.txt)",
          
        ]
    } 
    
    tags = {
        Name = "StagesM"
    }

    

}

output "Stages_ip" {
    value = aws_instance.Stages.public_ip
}


resource "aws_security_group" "instance_sg" {
  name = "terraform-test-sg"
  
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_instance" "my_ec2" {
    key_name      = aws_key_pair.my_ec2.key_name
    ami           = "ami-0d6aecf0f0425f42a"
    instance_type = "t2.small"
    vpc_security_group_ids = [aws_security_group.instance_sg.id]
    connection {
        type        = "ssh"
        user        = "ubuntu"
        private_key = file("./terraform")
        host        = self.public_ip
    }

       provisioner "file" {
        source      = "jenkins.sh"
        destination = "jenkins.sh"
    }

     provisioner "file" {
        source      = "ip_address.txt"
        destination = "ip_address.txt"
    }
    
       provisioner "file" {
        source      = "ip_address1.txt"
        destination = "ip_address1.txt"
    }
   provisioner "remote-exec" {
     
          inline = [
          " sudo sh jenkins.sh $(awk 'NR==1' ip_address.txt)",
          
        ]
    }   

tags = {
        Name = "JenkinsM"
    }

}
output "Jenkins_ip" {
    value = aws_instance.my_ec2.public_ip
}






resource "aws_security_group" "instance_sg2" {
  name = "terraform-kubernetes-sg"
  
    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }



    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }


  ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
      ingress {
        from_port = 8443
        to_port = 8443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
         ingress {
        from_port = 32000
        to_port = 32000
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "Kubernetes" {
    key_name      = aws_key_pair.my_ec2.key_name
    ami           = "ami-0d6aecf0f0425f42a"
    instance_type = "t2.medium"
    vpc_security_group_ids = [aws_security_group.instance_sg2.id]
    connection {
        type        = "ssh"
        user        = "ubuntu"
        private_key = file("./terraform")
        host        = self.public_ip
    }
    
         provisioner "file" {
        source      = "ip_address.txt"
        destination = "ip_address.txt"
    }
    
       provisioner "file" {
        source      = "kubernetesF.sh"
        destination = "/tmp/kubernetesF.sh"
    }
    
          provisioner "remote-exec" {
     
          inline = [
          "chmod +x  /tmp/kubernetesF.sh",
          "sudo sh /tmp/kubernetesF.sh ${aws_instance.my_ec21.public_ip} args",
          
        ]
    } 
 
    
    tags = {
        Name = "ProductionM"
    }

    

}

output "kubernetes_ip" {
    value = aws_instance.Kubernetes.public_ip
}







